##### Create POSTGRESDB
```
docker run -d \
--name postgres \
-p 5432:5432 \
-e POSTGRES_USER=user \
-e POSTGRES_PASSWORD=abcd123456 \
postgres:14.1
```

##### Create POSTGRESDB
```
# get in container - postgres
$ docker exec -it postgres bash

# get in postgresDB
$ psql -U user -W

# create a testing DB
$ CREATE DATABASE demo;

# switch to DB 'demo'
$ \c demo;

# after execute spring boot flyway, list tables
$ \dt

# drop table if you want to do other testing
$ DROP TABLE flyway_schema_history;
```

##### run with commands
```
# run on spring boot run via spring boot plugin
$ mvn spring-boot:run

# run on maven compile/package build via flyway plugin
$ mvn compile flyway:migrate -Dflyway.url=postgresql://localhost:5432/demo -Dflyway.user=user -Dflyway.password=abcd123456

# run on maven compile/package via flyway plugin using config file
$ mvn compile flyway:migrate -Dflyway.configFiles=flywayConfig.conf
```

##### reference

[flywaydb](https://flywaydb.org/documentation/)

[flyway-in-practice](https://blog.waterstrong.me/flyway-in-practice/)

[best-practices-using-flyway-for-database-migrations](https://dbabulletin.com/index.php/2018/03/29/best-practices-using-flyway-for-database-migrations/)
